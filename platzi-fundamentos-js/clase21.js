function Persona(nombre, apellido, altura){
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}

Persona.prototype.saludar = function (){
    if(this.altura > 1.75){
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy alto, mido ${this.altura}`)
    }else{
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy chaparro, mido ${this.altura}`)
    }
}

var victor = new Persona('Victor', 'Tapia', 1.78)
var erika = new Persona('Erika', 'Luna', 1.6)
var arturo = new Persona('Arturo', 'Martinez', 1.82)
