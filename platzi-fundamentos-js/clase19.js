var victor = {
    nombre: 'Victor',
    apellido: 'Tapia',
    altura: 1.78
}

var vicky = {
    nombre: 'Vicky',
    apellido: 'Zapata',
    altura: 1.56
}

var paula = {
    nombre: 'Paula',
    apellido: 'Barros',
    altura: 1.66
}

var javier = {
    nombre: 'Javier',
    apellido: 'Alonso',
    altura: 1.72
}

var dario = {
    nombre = 'Dario',
    Apellido = 'Juarez',
    altura = 1.75
}

//const esAlta = persona => personas.altura > 1.8
const esAlta = ({ altura }) => altura > 1.8

var personas = [victor, vicky, paula, javier, dario]

var personasAltas = personas.filter(esAlta)
/* var personasAltas = personas.filter(function (persona){
    return persona.altura > 1.8
}) */

const pasaAlturaACms = persona => ({
        ... persona,
        altura: persona.altura * 100
})

var personasCms = personas.map(pasaAlturaACms)