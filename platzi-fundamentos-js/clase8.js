//var nombreVictor = 'Víctor'
//var nombreDario = 'Dario'

var victor = {
    nombre: 'Victor',
    apellido: 'Tapia',
    edad: 24
}

var dario = {
    nombre: 'Dario',
    apellido: 'Susnisky',
    edad: 27
}

function imprimirNombreEnMayusculas({persona}){
    //var nombre = persona.nombre
    var {nombre} = persona
    console.log(nombre.toUpperCase())
}

//function imprimirNombreEnMayusculas(persona){
//    console.log(persona.nombre.toUpperCase())
//}

imprimirNombreEnMayusculas(victor)
imprimirNombreEnMayusculas(dario)
imprimirNombreEnMayusculas({nombre : 'pepito'})

function cumpleanos(persona){
    return{
        ...persona,
        edad: persona.edad + 1
    }
}