var victor = {
    nombre: 'Victor',
    apellido: 'Tapia',
    edad: 24,
    ingeniero: true,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: false,
    gamer: true
}

function imprimirProfesiones(persona){
    console.log(`${persona.nombre} es:`)

    if(persona.ingeniero){
        console.log('Ingeniero')
    }

    if(persona.cocinero){
        console.log('Cocinero')
    }

    if(persona.cantante){
        console.log('Cantante')
    }
    if(persona.dj){
        console.log('DJ')
    }
    if(persona.guitarrista){
        console.log('Guitarrista')
    }
    if(persona.gamer){
        console.log('Gamer')
    }
}

function imprimirSiEsMayorDeEdad(persona){
    if(persona.edad >= 18){
        console.log('Sì es mayor de edad')
    }else{
        console.log('No es mayor de edad')
    }
}

imprimirSiEsMayorDeEdad(victor)